//
//  Utils.m
//  Objective_C_5
//
//  Created by Echo Of Darkness on 10/30/19.
//  Copyright © 2019 Echo Of Darkness. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (void)_swap:(NSMutableArray *)arr i:(int)i j:(int)j
{
    [arr replaceObjectAtIndex:i withObject:[NSNumber numberWithInt:[[arr objectAtIndex:i] intValue] + [[arr objectAtIndex:j] intValue]]];
    [arr replaceObjectAtIndex:j withObject:[NSNumber numberWithInt:[[arr objectAtIndex:i] intValue] - [[arr objectAtIndex:j] intValue]]];
    [arr replaceObjectAtIndex:i withObject:[NSNumber numberWithInt:[[arr objectAtIndex:i] intValue] - [[arr objectAtIndex:j] intValue]]];
}

+(int)factorial:(int)n
{
    return n > 1 ? [Utils factorial:(n - 1)] * n : 1;
}

+ (NSMutableArray *)bobleSortNSNumbers:(NSMutableArray *)arr
{
    bool flag;
    do {
        flag = false;
        for (int i = 0; i < [arr count] - 1; i++)
        {
            if ([arr objectAtIndex:i] > [arr objectAtIndex:(i + 1)]) {
                flag = true;
                [Utils _swap:arr i:i j:i+1];
            }
        }
    } while(flag);
    return arr;
}

+ (NSMutableArray *)insertionSortNSNumbers:(NSMutableArray *)arr
{
    for(int i = 1; i < [arr count]; i++)
    {
        for(int j = i; j > 0; j--)
        {
            if ([arr objectAtIndex:j] < [arr objectAtIndex:(j - 1)]) {
                [Utils _swap:arr i:j j:(j - 1)];
                continue;
            } break;
        }
    }
    return arr;
}

+ (NSString *)analyzerSymbol:(char) ch
{
    if ((int) ch >= (int)'0' && (int) ch <= '9' ) {
        return [NSString stringWithFormat:@"It is number!"];
    } else if ((int) ch >= (int) 'a' && ch <= (int) 'z') {
        return [NSString stringWithFormat:@"It is character from interval [a-z]"];
    } else if ((int) ch >= (int) 'A' && ch <= (int) 'Z') {
        return [NSString stringWithFormat:@"It is character from interval [A-Z]"];
    } else {
        return @"Undefined";
    }
}

+ (NSDictionary *)analyzerString:(NSString *) string
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    for (int i = 0; i < [string length]; i++)
    {
        [dict setObject:[dict objectForKey:[NSNumber numberWithChar: [string characterAtIndex:i]]]?[NSNumber numberWithInt:([[dict objectForKey:[NSNumber numberWithChar: [string characterAtIndex:i]]]intValue] + 1)]:@0 forKey:[NSNumber numberWithChar: [string characterAtIndex:i]]];
    }
    return [dict copy];
}

+ (NSString *)displayTable:(NSDictionary *) dict
{
    for (NSNumber* item in [dict allKeys])
    {
        NSLog(@"{%c: %d}", [item charValue], [[dict objectForKey:item] intValue]);
    }
    return nil;
}

+ (float) calculatorOp1:(int)op1 op2:(int)op2 op:(char)op
{
    switch (op) {
        case '+':
            return op1 + op2;
        case '-':
            return op1 - op2;
        case '*':
            return op1 * op2;
        case '/':
            return op1 / op2;
        case '%':
            return op1 % op2;
        case '^':
            return [Utils _pow:op1 n:op2];
    } return 0;
}

+ (int) _pow:(int)a n:(int)n
{
    return n > 0 ? a * [Utils _pow:a n:(n - 1)] : 1;
}

// [x, y, radius], ...
+ (NSArray *)getLargeCircleContainedInDot:(NSArray *)arr x:(int)x y:(int)y
{
    NSMutableArray* currentCircle = [[NSMutableArray alloc] initWithObjects:@0, @0, @0, nil];
    for(int i = 0; i < [arr count]; i+=3)
    {
        int r = [Utils _pow:([[arr objectAtIndex:i] intValue] - x) n:2] + [Utils _pow:([[arr objectAtIndex:(i + 1)] intValue] - y) n:2];
        if (r <= [Utils _pow:[[arr objectAtIndex:(i + 2)] intValue] n:2] && [[currentCircle objectAtIndex:2] intValue] < [[arr objectAtIndex:(i + 2)] intValue] ) {
            [currentCircle replaceObjectsInRange:NSMakeRange(0, 3) withObjectsFromArray:arr range:NSMakeRange(i, 3)];
        }
    }
    return [currentCircle copy];
}

@end
