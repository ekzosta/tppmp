//
//  main.m
//  Objective_C_5
//
//  Created by Echo Of Darkness on 10/30/19.
//  Copyright © 2019 Echo Of Darkness. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Utils.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSLog(@"5! = %d", [Utils factorial:5]);
        NSMutableArray *arr = [NSMutableArray arrayWithObjects:@5, @4, @3, @2, @1, @0, nil];
        NSLog(@"Our array: %@", arr);
        NSLog(@"After boble sort: %@", [Utils bobleSortNSNumbers:arr]);
        arr = [NSMutableArray arrayWithObjects:@5, @4, @3, @2, @1, @0, nil];
        NSLog(@"Our array: %@", arr);
        NSLog(@"After insertion sort: %@", [Utils insertionSortNSNumbers:arr]);
        NSLog(@"Result: %@", [Utils analyzerSymbol:'0']);
        NSLog(@"Result: %@", [Utils analyzerSymbol:'a']);
        NSLog(@"Result: %@", [Utils analyzerSymbol:'D']);
        NSLog(@"Result: %@", [Utils analyzerSymbol:'-']);
        NSString *str = @"Hello world!";
        NSLog(@"Result for string: %@", str);
        [Utils displayTable: [Utils analyzerString:str]];
        NSLog(@"2^10 = %d", (int)[Utils calculatorOp1:2 op2:10 op:'^']);
        NSLog(@"(x, y, radius) %@", [Utils getLargeCircleContainedInDot:[NSArray arrayWithObjects:@0, @0, @5, @2, @2, @10, @(-5), @5, @50, nil] x:0 y:0]);
    }
    return 0;
}
