//
//  Utils.h
//  Objective_C_5
//
//  Created by Echo Of Darkness on 10/30/19.
//  Copyright © 2019 Echo Of Darkness. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+ (int)factorial:(int)n;
+ (NSMutableArray *)bobleSortNSNumbers:(NSMutableArray *)arr;
+ (NSMutableArray *)insertionSortNSNumbers:(NSMutableArray *)arr;
+ (void)_swap:(NSMutableArray *)arr i:(int)i j:(int)j;
+ (NSString *)analyzerSymbol:(char) ch;
+ (NSDictionary *)analyzerString:(NSString *) string;
+ (NSString *)displayTable:(NSDictionary *) dict;
+ (float) calculatorOp1:(int)op1 op2:(int)op2 op:(char)op;
+ (int) _pow:(int)a n:(int)n;
+ (NSArray *)getLargeCircleContainedInDot:(NSArray *)arr x:(int)x y:(int)y;

@end
