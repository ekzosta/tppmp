//
//  main.swift
//  Lab1
//
//  Created by Martin on 26.10.2019.
//  Copyright © 2019 Martin Reparuk. All rights reserved.
//
//Laboratorna 1

import Foundation
print ("Завдання 2.1-2.10")
var number = 0
var number1 :int_fast8_t = 12
var number2 :int_fast16_t = -100
var number3 :Int = 0x80
var number4 :int_fast16_t = 10
var number5 :int_fast64_t = 1000
var number6 :Float = 10235.34
var number7 :Character = "a"
var number8 :String = "Hello World"
var number9 :Bool = true
var number10:int_fast8_t = 12
print(number1,"\n",number2,"\n",number3,"\n",number4,"\n",number5,"\n",number6,"\n",number7,"\n",number8,"\n",number9 )

print ("Завдання 3.1")

var str :String = "Hello World. This is Swift programming language"
var size=str.count
print(str + "\nString length = " + String(size))
var newString = str.replacingOccurrences(of: "i", with: "u")

print("Завдання 3.2\n" + newString)

print("Завдання 3.3")
str.remove(at: str.index(str.startIndex, offsetBy: 4))
str.remove(at: str.index(str.startIndex, offsetBy: 7))
str.remove(at: str.index(str.startIndex, offsetBy: 10))
print( str )

print("Завдання 3.4\n" + str + " (modified)" )
print("Завдання 3.5\n" + String(str.isEmpty))
str.insert(".", at: str.endIndex)
print("Завдання 3.6\n" + str)
print("Завдання 3.7")
if str.hasPrefix("Hello")
{
    print("Prefix exists")
}
print("Завдання 3.8" )
if str.hasSuffix("world.")
{
    print("Suffix exists")
}

print("Завдання 3.9" )
str.insert("-", at: str.index(str.startIndex, offsetBy: 10))
print(str)

print("Завдання 3.10")
var newSrting2 = str.replacingOccurrences(of: "This is", with: "It is")
print(newSrting2)

print("Завдання 3.11")
var index1 = str.index(str.startIndex, offsetBy: 10)
var index2 = str.index(str.startIndex, offsetBy: 15)
print(str[index1],str[index2])

print("Завдання 3.12")
print(str[index1..<index2])

print("Завдання 4.1")
var integerNumber:Optional<Int> = Optional.none

print("Завдання 4.2")
var decimalNumber :Optional<Float> = Optional.none

print("Завдання 4.3")
integerNumber = 4
print(integerNumber!)

print("Завдання 4.4")
for i in 1...4
{
integerNumber=integerNumber!+1
print(integerNumber!)
}

print("Завдання 4.5")
var newInteger = -integerNumber!
print(newInteger)

print("Завдання 4.6")
decimalNumber = Float(integerNumber!)
print(decimalNumber!)

print("Завдання 4.7")
var pairOrValues :Int?
if pairOrValues == integerNumber!
{
    print(pairOrValues!)
}else
{
    print("Error")
}

print("Завдання 4.8")
pairOrValues = Int(decimalNumber!)
if pairOrValues! ==  Int(decimalNumber!)
{
    print(pairOrValues!)
}else
{
    print("Error")
}

print("Завдання 4.9")

if pairOrValues != nil
{
    let c = true
    print("Value: " + String(c))
    
}

print("Завдання 4.10")

if decimalNumber != nil
{
    print(decimalNumber!)
}



