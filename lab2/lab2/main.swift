//
//  main.swift
//  lab2
//
//  Created by Alex on 10/27/19.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

//Zav1
//1
var fibArray : [Int] = [0,1,1,2,3,5,8,13,21,34]
//2
//let revArr : [Int]  = Array(fibArray.reverse())
//в новій версії не робить
//3
var snglArray : [Int] = [81,32,63,95,85,13,99,44,56,84,95,43,76,93,37,45,62,72,57,90,56,67,87,98,08,39,1]
//4
print(snglArray.count)
//5
print(snglArray[10])
//6
for i in 15...20 {
    print(snglArray[i], terminator: ",")
}
//7
print("")
var rptArray : [Int] = [1,2,3,4,5,6,7,8,9,10]
for i in 0...9 {
    rptArray[i]=snglArray[10]
}
//8
var oddArray = [1,3,7,5]
//9
oddArray.append(11)
//10
var podarr = [[15,17,19]]
oddArray = oddArray + podarr[0]
//11
print(oddArray)
//12
print(oddArray.count)
oddArray = oddArray + oddArray
for i in 5...8 {
    oddArray.remove(at: i)
}
print(oddArray)
//13
print(oddArray.removeLast())
//14
//-непоняв
//15
for i in oddArray {
    //if (oddArray[i]==3){oddArray.remove(at: i)}
}
//16
var tryis : Int = 0
for i in oddArray {
    //if (oddArray[i]==3){tryis+=1}
}
if(tryis>0){print("в массиве есть число 3")}else {print("в массиве нет числа 3")}
//17
//тоже непоняв
//Zav2
//1
var chChar : Set<Character> = ["a","b","c","d"]
//2
var mChSet = chChar
//3
print(mChSet.count)
//4
mChSet.insert("e")
//5
var SrtChSet = mChSet.sorted()
//6
mChSet.insert("f")
print(mChSet.remove("f"))
//7
//print(mChSet.removeAtIndex(1))
//8
/////????
//9
mChSet.insert("a")
//10
var aSet : Set<String> = [ "One","Two","Three","1","2"]
var bSet : Set<String> = [ "1","2","3","One","Two"]
//11
var p11 :Set<String> = aSet.intersection(bSet)
//12
var p12_1 = aSet.subtract(bSet)
//var p12_1 : Set<String> = aSet.subtract(bSet)
var p12_2 = bSet.subtract(aSet)
//var p12_2 : Set<String> = bSet.subtract(aSet)
//13
//var p13_1 = aSet.symmetricDifference(bSet)
var p13_1 : Set<String> = aSet.symmetricDifference(bSet)
//var p13_2 = bSet.symmetricDifference(aSet)
var p13_2 : Set<String> = bSet.symmetricDifference(aSet)
//14
var p14 : Set<String> = aSet.union(bSet)
//15
var xSet : Set<Int> = [2,3,4]
var ySet : Set<Int> = [1,2,3,4,5,6]
var zSet : Set<Int> = [3,4,2]
var x1Set : Set<Int> = [5,6,7]
//16
//var test: Set<Int> =
//17

//18
if(xSet==zSet){print("Yes")}else{print("false")}
//19

//20

//zav3
//1
var nDict = [1:"One", 2:"Two", 3:"Three", 4:"Four", 5:"Five"]
//2
print(nDict[3])
//3
//print(nDict[4])
//4
var mNDict = nDict
//5
mNDict[7] = "Six"
mNDict[6] = "Seven"
//6
mNDict.updateValue("Six", forKey: 6)
mNDict.updateValue("Seven", forKey: 7)
//7
mNDict.removeValue(forKey: 5)
//8
//mNDict.removeValue(forKey: 4)
//9
//func distance(from start: Dictionary< 1,"One">.Index, to end: Dictionary< 7,"Seven">.Index) -> Int
//10

//11

//12
print(mNDict.count)
//13
