//
//  Son.h
//  Objective_C_3
//
//  Created by Echo Of Darkness on 10/27/19.
//  Copyright © 2019 Echo Of Darkness. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"
@class Person;

@interface Son : NSObject
{
    Person *_parent;
    NSString *_name;
}
- (id)initWithName:(NSString *)name;
- (Person *)parent;
- (void)setParent:(Person *)person;
- (NSString *)name;
- (void)setName:(NSString *)name;
- (void)deactivateObject;
@end
