//
//  Person.h
//  Objective_C_3
//
//  Created by Echo Of Darkness on 10/27/19.
//  Copyright © 2019 Echo Of Darkness. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Son.h"
@class Son;

@interface Person : NSObject
{
    Son* _son;
    NSString* _name;
}
- (id) initWithName:(NSString*) name;
- (Son *)son;
- (void)setSon: (Son *)son;
- (NSString *)name;
- (void)setName: (NSString *)name;
- (void)deactivateObject;
@end
