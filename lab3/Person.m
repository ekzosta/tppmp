//
//  Person.m
//  Objective_C_3
//
//  Created by Echo Of Darkness on 10/27/19.
//  Copyright © 2019 Echo Of Darkness. All rights reserved.
//

#import "Person.h"

@implementation Person: NSObject


- (instancetype)init
{
    if (self = [super init]) {
        NSLog(@"Successfully Person init");
    }
    return self;
}

- (id)initWithName:(NSString *)name
{
    self = [self init];
    [self setName:name];
    return [super init];
}

- (void) setSon:(Son *)son
{
    _son = son;
}

- (Son *) son
{
    return _son;
}

- (NSString *)name
{
    return _name;
}

- (void)setName:(NSString *)name
{
    _name = name;
}

- (NSString* )description
{
    return [NSString stringWithFormat:@"People<name:%@> has son [%@]", [self name], [_son name]];
}

- (void)dealloc
{
    NSLog(@"Successfully Person dealloc");
}

- (void)deactivateObject
{
    _son = nil;
}
@end
