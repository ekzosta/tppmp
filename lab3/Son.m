//
//  Son.m
//  Objective_C_3
//
//  Created by Echo Of Darkness on 10/27/19.
//  Copyright © 2019 Echo Of Darkness. All rights reserved.
//

#import "Son.h"

@implementation Son: NSObject

- (instancetype)init
{
    if (self = [super init]) {
        NSLog(@"Successfully Son init");
    }
    return self;
}

- (id)initWithName:(NSString *)name
{
    self = [self init];
    [self setName: name];
    return [super init];
}
- (Person *)parent
{
    return _parent;
}

- (void)setParent:(Person *)person
{
    _parent = person;
}

- (NSString *)name
{
    return _name;
}

- (void)setName:(NSString *)name
{
    _name = name;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"Son<%@> has parent[%@]", [self name], [_parent name]];
}

- (void) deactivateObject
{
    [self setParent:nil];
}

- (void)dealloc
{
    NSLog(@"Successfully Son dealloc");
}

@end
