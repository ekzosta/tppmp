//
//  MyNumber.m
//  Objective_C_3
//
//  Created by Echo Of Darkness on 10/29/19.
//  Copyright © 2019 Echo Of Darkness. All rights reserved.
//

#import "MyNumber.h"

@implementation MyNumber

- (id)initWithValue:(NSNumber *)value
{
    _value = [value retain];
    return [super init];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"MyNumber<%li>", [[self value] longValue]];
}

- (void)setValue:(NSNumber *) value
{
    [_value release];
    _value = [value retain];
}

- (NSNumber *)value
{
    return _value;
}

- (void)dealloc
{
    NSLog(@"Work dealloc %@", self);
    [_value release];
    [super dealloc];
}

+ (MyNumber *)myNumberWithInt:(int)value
{
    return [[[MyNumber alloc] initWithValue:[NSNumber numberWithInt:value]] autorelease];
}

@end
