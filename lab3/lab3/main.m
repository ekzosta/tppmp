//
//  main.m
//  Objective_C_3
//
//  Created by Echo Of Darkness on 10/27/19.
//  Copyright © 2019 Echo Of Darkness. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"
#import "Son.h"
#import "MyNumber.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Person *person = [[Person alloc] initWithName: @"Alex"];
        Son *son = [[Son alloc] initWithName: @"John"];
        [person setSon: son];
        [son setParent: person];
        NSLog(@"%@", person);
        NSLog(@"%@", son);
        //[son deactivateObject];
        [person deactivateObject];
        
        MyNumber *number = [[MyNumber alloc] initWithValue:@4];
        NSLog(@"%@", number);
        [MyNumber myNumberWithInt:3];
    }
    return 0;
}
