//
//  MyNumber.h
//  Objective_C_3
//
//  Created by Echo Of Darkness on 10/29/19.
//  Copyright © 2019 Echo Of Darkness. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyNumber : NSObject
{
    NSNumber *_value;
}
- (id)initWithValue:(NSNumber *)value;
- (void)setValue:(NSNumber *)value;
- (NSNumber *)value;

+ (MyNumber *)myNumberWithInt:(int)value;
@end
