//
//  Rational.h
//  Objective_C_4
//
//  Created by Echo Of Darkness on 10/30/19.
//  Copyright © 2019 Echo Of Darkness. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Rational : NSObject
{
    NSNumber *_numerator;
    NSNumber *_denominator;
}
@property (strong, nonatomic) NSNumber *numerator;
@property (strong, nonatomic) NSNumber *denominator;

- (instancetype)initWithNumerator:(NSNumber *)numerator denominator:(NSNumber *)denominator;
- (Rational *)_transformation;
@end
