//
//  Rational.m
//  Objective_C_4
//
//  Created by Echo Of Darkness on 10/30/19.
//  Copyright © 2019 Echo Of Darkness. All rights reserved.
//

#import "Rational.h"

int NSD(int n, int m) {
    n = n < m ? m + n : n;
    m = n < m ? n - m : m;
    n = n < m ? n - m : n;
    while(n != m) {
        if (n > m) n -= m;
        else m -= n;
    }
    return n;
}

@implementation Rational
@synthesize numerator = _numerator;
@synthesize denominator = _denominator;

- (instancetype)initWithNumerator:(NSNumber *)numerator denominator:(NSNumber *)denominator
{
    _numerator = numerator;
    _denominator = denominator;
    return [super init];
}

- (Rational *)_transformation
{
    int n = 1, m;
    do {
        [self setNumerator:[NSNumber numberWithInt: ([[self numerator] intValue] / (n == 0 ? 1 : n))]];
        [self setDenominator:[NSNumber numberWithInt: ([[self denominator] intValue] / (n == 0 ? 1 : n))]];
        if (n == 0) break;
        n = [[self numerator] intValue];
        m = [[self denominator] intValue];
    } while((n = NSD(n, m)) != 1);
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"(numerator, denominator): (%li, %li)", [[self numerator] longValue], [[self denominator] longValue]];
}
@end
