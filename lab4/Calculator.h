//
//  Calculator.h
//  Objective_C_4
//
//  Created by Echo Of Darkness on 10/30/19.
//  Copyright © 2019 Echo Of Darkness. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Rational.h"

@interface Calculator : NSObject
{
    Calculator *_calculator;
}
- (id)init __attribute__((unavailable("alloc not available")));
- (id)copy __attribute__((unavailable("alloc not available")));

+ (id)alloc __attribute__((unavailable("alloc not available")));
+ (id)new __attribute__((unavailable("alloc not available")));

+ (instancetype)calculator;

- (Rational *)add:(Rational *)number1 number:(Rational *)number2;
- (Rational *)sub:(Rational *)number1 number:(Rational *)number2;
- (Rational *)div:(Rational *)number1 number:(Rational *)number2;
- (Rational *)mul:(Rational *)number1 number:(Rational *)number2;

@end
