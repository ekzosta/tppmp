//
//  Calculator.m
//  Objective_C_4
//
//  Created by Echo Of Darkness on 10/30/19.
//  Copyright © 2019 Echo Of Darkness. All rights reserved.
//

#import "Calculator.h"

@implementation Calculator
+ (instancetype)calculator;
{
    static Calculator * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [super new];
    });
    return instance;
}

- (Rational *)add:(Rational *)number1 number:(Rational *)number2
{
    return [[[Rational alloc] initWithNumerator:[NSNumber numberWithLong:([[number1 numerator] longValue] * [[number2 denominator] longValue] + [[number2 numerator] longValue] * [[number1 denominator] longValue])] denominator:[NSNumber numberWithLong:([[number1 denominator] longValue] * [[number2 denominator] longValue])]] _transformation];
}

- (Rational *)sub:(Rational *)number1 number:(Rational *)number2
{
    return [[[Rational alloc] initWithNumerator:[NSNumber numberWithLong:([[number1 numerator] longValue] * [[number2 denominator] longValue] - [[number2 numerator] longValue] * [[number1 denominator] longValue])] denominator:[NSNumber numberWithLong:([[number1 denominator] longValue] * [[number2 denominator] longValue])]] _transformation];
}

- (Rational *)mul:(Rational *)number1 number:(Rational *)number2
{
    return [[[Rational alloc] initWithNumerator:[NSNumber numberWithLong:([[number1 numerator] longValue] * [[number2 numerator] longValue])] denominator:[NSNumber numberWithLong:([[number1 denominator] longValue] * [[number2 denominator] longValue])]] _transformation];
}

- (Rational *)div:(Rational *)number1 number:(Rational *)number2
{
    return [[[Rational alloc] initWithNumerator:[NSNumber numberWithLong:([[number1 numerator] longValue] / [[number2 numerator] longValue])] denominator:[NSNumber numberWithLong:([[number1 denominator] longValue] / [[number2 denominator] longValue])]] _transformation];
}

@end
