//
//  main.m
//  Objective_C_4
//
//  Created by Echo Of Darkness on 10/30/19.
//  Copyright © 2019 Echo Of Darkness. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Calculator.h"
#import "Rational.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSLog(@"(1/2 + 1/2) = %@", [[Calculator calculator] add:[[Rational alloc] initWithNumerator:@1 denominator:@2] number:[[Rational alloc]initWithNumerator:@1 denominator:@2] ]);
        NSLog(@"(3/2 - 1/4) = %@", [[Calculator calculator] sub:[[Rational alloc] initWithNumerator:@3 denominator:@2] number:[[Rational alloc]initWithNumerator:@1 denominator:@4] ]);
        NSLog(@"(1/2 * 1/2) = %@", [[Calculator calculator] mul:[[Rational alloc] initWithNumerator:@1 denominator:@2] number:[[Rational alloc]initWithNumerator:@1 denominator:@2] ]);
        NSLog(@"(1/4 / 1/2) = %@", [[Calculator calculator] div:[[Rational alloc] initWithNumerator:@1 denominator:@4] number:[[Rational alloc]initWithNumerator:@1 denominator:@2] ]);
        NSLog(@"(1/2 + 1/3 * 3)= %@", [[Calculator calculator] add:[[Calculator calculator] mul:[[Rational alloc] initWithNumerator:@1 denominator:@3] number:[[Rational alloc] initWithNumerator:@3 denominator:@1]] number:[[Rational alloc] initWithNumerator:@1 denominator:@2]]);
    }
    return 0;
}
